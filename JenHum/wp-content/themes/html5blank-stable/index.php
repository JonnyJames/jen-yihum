<?php 

// Template Name: Home

get_header(); ?>

	<main role="main">
		<!-- section -->
		<h1>THIS IS TEMPLATE "HOME"</h1>
		<section>

			<h1><?php _e( 'Latest Posts', 'html5blank' ); ?></h1>

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
